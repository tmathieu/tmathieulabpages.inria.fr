Website made with emacs and ox-hugo, published page can be found here: https://tmathieu.gitlabpages.inria.fr/homepage/home
To transform org from hugo, write org page in content-org and export as hugo (=C-c C-e H H=). 

For hugo preview, use 


```
hugo server --buildDrafts --navigateToChanged
```

To just build, use `hugo` and it will build the website in the public folder.

The theme is forked from flamingo theme from https://github.com/mrprofessor/rudra.dev

