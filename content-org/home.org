#+hugo_base_dir: ../
#+hugo_section: home
#+author: Timothée Mathieu

* Home
   :PROPERTIES:
   :EXPORT_FILE_NAME: _index
   :EXPORT_HUGO_LAYOUT: single
   :CUSTOM_ID: main
   :END:
[[file:../histo.png]]
Researcher at [[https://team.inria.fr/scool/][Scool]].
***  Keywords

Robust Statistics, Sequential tests, M-estimation, Statistical Reproducibility, Reinforcement Learning, Machine Learning.
***  Contact

Inria Lille, France.

*Mail:* timothee.mathieu at inria.fr
