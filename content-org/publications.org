#+hugo_base_dir: ../
#+hugo_section: publications
#+author: Timothée Mathieu

* Publications
   :PROPERTIES:
   :EXPORT_FILE_NAME: _index
   :EXPORT_HUGO_LAYOUT: single
   :CUSTOM_ID: main
   :END:

*** Journal papers
- Concentration study of M-estimators using the influence function, in *Electronic Journal of Statistics*  (2022) ([[https://projecteuclid.org/journals/electronic-journal-of-statistics/volume-16/issue-1/Concentration-study-of-M-estimators-using-the-influence-function/10.1214/22-EJS2030.full][project euclid]]).
- Excess risk bounds in robust empirical risk minimization, with S. Minsker, (2020), in *Information and Inference: A Journal of the IMA* ([[https://arxiv.org/abs/1910.07485][arxiv]]).
- Robust classification via MOM minimization, with M. Lerasle and G. Lecué, (2020), published in *Machine Learning* ([[https://arxiv.org/abs/1808.03106][arxiv]]).
- Bandits Corrupted by Nature: Lower Bounds on Regret and Robust Optimistic Algorithm, with O. Maillard and D. Basu, (2022), to appear in *TMLR* ([[https://arxiv.org/abs/2203.03186][arxiv]]).
*** Conference papers
- MONK -- Outlier-Robust Mean Embedding Estimation by Median-of-Means, with  Matthieu Lerasle, Zoltan Szabo, and Guillaume Lecué, (2019) in *ICML* ([[http://proceedings.mlr.press/v97/lerasle19a.html][pmlr]], [[https://arxiv.org/abs/1802.04784][arxiv]]).
- Topics in robust statistical learning, with  Claire Brécheteau, Edouard Genetay and Adrien Saumard, published in *ESAIM: PROCEEDINGS AND SURVEYS* ([[https://hal.archives-ouvertes.fr/hal-03605702v1][HAL]]).
- CRIMED: Lower and Upper Bounds on Regret for Bandits with Unbounded Stochastic Corruption, with S. Agrawal, O. Maillard and D. Basu, (2024) in *ALT*, ([[https://arxiv.org/abs/2309.16563][arxiv]]).
*** Pre-prints
- AdaStop: sequential testing for efficient and reliable comparisons of Deep RL Agents, with Riccardo Della Vecchia, Alena Shilova, Matheus Centa de Medeiros, Hector Kohler, Odalric-Ambrym Maillard, Philippe Preux (2023), ([[https://arxiv.org/abs/2306.10882][arxiv]]).
- Information Lower Bounds for Robust Mean Estimation, with Rémy Degenne (2024) ([[https://arxiv.org/abs/2403.01892][arxiv]]).
*** PhD
M-estimation and Median of Means applied to statistical learning, Ph.D. dissertation (2021) ([[https://hal-universite-paris-saclay.archives-ouvertes.fr/tel-03132439v1][HAL]]).
