+++
title = "Home"
author = ["Timothée Mathieu"]
layout = "single"
draft = false
+++

![](../histo.png)
Researcher at [Scool](https://team.inria.fr/scool/).


## Keywords {#keywords}

Robust Statistics, Sequential tests, M-estimation, Statistical Reproducibility, Reinforcement Learning, Machine Learning.


## Contact {#contact}

Inria Lille, France.

**Mail:** timothee.mathieu at inria.fr
