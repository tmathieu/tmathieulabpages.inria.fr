+++
title = "Publications"
author = ["Timothée Mathieu"]
layout = "single"
draft = false
+++

## Journal papers {#journal-papers}

-   Concentration study of M-estimators using the influence function, in **Electronic Journal of Statistics**  (2022) ([project euclid](https://projecteuclid.org/journals/electronic-journal-of-statistics/volume-16/issue-1/Concentration-study-of-M-estimators-using-the-influence-function/10.1214/22-EJS2030.full)).
-   Excess risk bounds in robust empirical risk minimization, with S. Minsker, (2020), in **Information and Inference: A Journal of the IMA** ([arxiv](https://arxiv.org/abs/1910.07485)).
-   Robust classification via MOM minimization, with M. Lerasle and G. Lecué, (2020), published in **Machine Learning** ([arxiv](https://arxiv.org/abs/1808.03106)).
-   Bandits Corrupted by Nature: Lower Bounds on Regret and Robust Optimistic Algorithm, with O. Maillard and D. Basu, (2022), to appear in **TMLR** ([arxiv](https://arxiv.org/abs/2203.03186)).
-   AdaStop: adaptive statistical testing for sound comparisons of Deep RL agents, with Riccardo Della Vecchia, Alena Shilova, Matheus Centa de Medeiros, Hector Kohler, Odalric-Ambrym Maillard, Philippe Preux (2023), **TMLR** ([arxiv](https://arxiv.org/abs/2306.10882), [openreview](https://openreview.net/forum?id=lXyZr9TLEU)).

## Conference papers {#conference-papers}

-   MONK -- Outlier-Robust Mean Embedding Estimation by Median-of-Means, with  Matthieu Lerasle, Zoltan Szabo, and Guillaume Lecué, (2019) in **ICML** ([pmlr](http://proceedings.mlr.press/v97/lerasle19a.html), [arxiv](https://arxiv.org/abs/1802.04784)).
-   Topics in robust statistical learning, with  Claire Brécheteau, Edouard Genetay and Adrien Saumard, published in **ESAIM: PROCEEDINGS AND SURVEYS** ([HAL](https://hal.archives-ouvertes.fr/hal-03605702v1)).
-   CRIMED: Lower and Upper Bounds on Regret for Bandits with Unbounded Stochastic Corruption, with S. Agrawal, O. Maillard and D. Basu, (2024) in **ALT**, ([arxiv](https://arxiv.org/abs/2309.16563)).


## Pre-prints {#pre-prints}


- Information Lower Bounds for Robust Mean Estimation, with Rémy Degenne (2024) ([arxiv](https://arxiv.org/abs/2403.01892)).
- Visual tests using several safe confidence intervals (2025) ([arxiv](https://arxiv.org/abs/2503.03567)).

## PhD {#phd}

M-estimation and Median of Means applied to statistical learning, Ph.D. dissertation (2021) ([HAL](https://hal-universite-paris-saclay.archives-ouvertes.fr/tel-03132439v1)).
