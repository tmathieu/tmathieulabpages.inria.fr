+++
title = "Software"
author = ["Timothée Mathieu"]
layout = "single"
draft = false
+++

## Statistical Reproducibility {#statistical-reproducibility}

-   We created [adastop](https://github.com/TimotheeMathieu/adastop), a cli tool and python library for efficient and reliable comparison of stochastic algorithms using non-parametric group sequential tests.


## Sequential testing &amp; Reinforcement learning {#sequential-testing-and-reinforcement-learning}

-   I head the development team of [rlberry](https://github.com/rlberry-py/rlberry) a reinforcement learning library for research and education.


## Robust statistics {#robust-statistics}

-   I contribute to [scikit-learn-extra](https://github.com/scikit-learn-contrib/scikit-learn-extra) library.
-   Notebook to illustrate my PhD: [colab link](https://colab.research.google.com/drive/1yyGCgmif1EXBNLBgM0DaZvPLyHuJW8zf?usp=sharing).
-   A python library for [M-estimation](https://github.com/TimotheeMathieu/RobustMeanEstimator).


## Misc {#misc}

-   [spf-py](https://github.com/TimotheeMathieu/spf-py) is a python cli tool for writing structured proofs in markdown, see an example [here](../np.html).
