+++
title = "Recent Talks"
author = ["Timothée Mathieu"]
layout = "single"
draft = false
+++

-   "Robust Multivariate Mean estimation with M-estimators" at the Séminaire Parisien de Statistiques (april 2024).
-   "Robust Multivariate Mean estimation with M-estimators" at the weekly Seminar at MISTEA lab in Montpellier (march 2024).
-   "Robust Multivariate Mean estimation with M-estimators" at the Conference on Statistical estimation in Saint-Étienne (12 to 14 June 2023).
-   "Robust study of consistent M-estimators using an optimal-transport distance", at MAS days (Modélisation Aléatoire et Statistique) in 2022.
